package dam.androidEdgar.u2p4conversor;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends LogActivity {

    // TODO: Ex4 no está en ningún sitio, ya que el texto desaparece al voltear el móvil sin haber
    //  puesto nada en el código, ya que la aplicación se "destruye"

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI(){

        TextView pulgada = findViewById(R.id.pulgada);
        TextView resultado = findViewById(R.id.resultado);
        Button buttonCM = findViewById(R.id.buttonCM);
        Button buttonPulg = findViewById(R.id.buttonPulg);

        buttonCM.setOnClickListener(view -> {
            try{
                resultado.setText(convertirpul(pulgada.getText().toString()));
            }catch(Exception e){
                Log.e("LogsConversor", e.getMessage());
            }
        });

        buttonPulg.setOnClickListener(view -> {
            try{
                pulgada.setText(convertircm(resultado.getText().toString()));
            }catch(Exception e){
                Log.e("LogsConversor", e.getMessage());
            }
        });
    }

    // TODO: Ex1 convierte las pulgadas en centímetros
    private String convertirpul(String pulgadaText){

        TextView ventana = findViewById(R.id.textMensaje);
        double pulgadaDouble = Double.parseDouble(pulgadaText);
        // TODO: Ex2 volvemos a dedjar invisible el texto del error
        if(pulgadaDouble > 0){
            ventana.setVisibility(View.INVISIBLE);
        }
        double pulgadaValue = Double.parseDouble(pulgadaText) * 2.54;
        // TODO: Ex2 aquí capturamos el error de que el número sea menor que 0 (en todo el try catch)
        try{
            if(pulgadaDouble <= 0){
                throw new Exception("Sólo números >= 1");
            }
        }catch(Exception e){
            Log.i(LogActivity.DEBUG_TAG + getLocalClassName(), "Sólo números >= 1");
            // TODO: Ex2 con esto lo que hago es dejar visible el texto que nos notifica que hay un error
            ventana.setVisibility(View.VISIBLE);
        }
        // TODO: Ex3 aquí notifica que hemmos usado determinado botón para efectuar la operación
        Log.i(LogActivity.DEBUG_TAG + getLocalClassName(), "botón de pulgadas pulsado");
        return String.valueOf(pulgadaValue);
    }


    // TODO: Ex1 convierte los centímetros en pulgadas
    private String convertircm(String cmText){

        TextView ventana = findViewById(R.id.textMensaje);
        double cmDouble = Double.parseDouble(cmText);
        // TODO: Ex2 volvemos a dedjar invisible el texto del error
        if(cmDouble > 0){
            ventana.setVisibility(View.INVISIBLE);
        }
        double cmValue = Double.parseDouble(cmText) / 2.54;
        // TODO: Ex2 aquí capturamos el error de que el número sea menor que 0 (en todo el try catch)
        try{
            if(cmDouble <= 0){
                throw new Exception("Sólo números >= 1");
            }
        }catch(Exception e){
            Log.i(LogActivity.DEBUG_TAG + getLocalClassName(), "Sólo números >= 1");
            // TODO: Ex2 con esto lo que hago es dejar visible el texto que nos notifica que hay un error
            ventana.setVisibility(View.VISIBLE);
        }
        // TODO: Ex3 aquí notifica que hemmos usado determinado botón para efectuar la operación
            Log.i(LogActivity.DEBUG_TAG + getLocalClassName(), "botón de cm pulsado");
            return String.valueOf(cmValue);
    }
}