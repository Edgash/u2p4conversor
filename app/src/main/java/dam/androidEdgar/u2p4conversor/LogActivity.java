package dam.androidEdgar.u2p4conversor;

import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

public class LogActivity extends AppCompatActivity {
    // TODO: Ex3 gracias a todo esto notificamos todos los ciclos de vida de la aplicación
    public static final String DEBUG_TAG = "LOG-";

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(DEBUG_TAG + getLocalClassName(), "onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(DEBUG_TAG + getLocalClassName(), "onStop");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(DEBUG_TAG + getLocalClassName(), "onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(DEBUG_TAG + getLocalClassName(), "onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(DEBUG_TAG + getLocalClassName(), "onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(DEBUG_TAG + getLocalClassName(), "onDestroy");
    }

}
